# Game of the Rope #

This is the RMI version of a client-server solution of the classic Game of the Problem. Communication between clients and servers is now implemented by the RMI abstraction, registring servers in the RMI registry and accessing all required methods by clients with stub interfaces.
This project is the third of three projects from the Distributed Systems course, from the 4th year of the Integrated Masters in Computers and Telematics Engineering (University of Aveiro).

### Main keywords ###
* Java
* Concurrency
* Explicit monitors
* Sockets
* Parallelism
* RMI

### Running ###

* Just run the deployment script run.sh

### Owners ###

The entire solution was developped by Rui Espinha Ribeiro ([Espinha](https://bitbucket.org/Espinha)) and David Silva ([dmpasilva](https://bitbucket.org/dmpasilva)).